/* Pipe between C and Python program
 * =================================
 *
 *  This is the worker process (slave)
 *
 *
 * About
 * -----
 *
 *  - The C program is the slave (worker process)
 *  - The Python program is the master (e.g. a GUI program)
 *  - The communication between the programs is realized by a named pipe
 *  - The master initiates a pipe
 *  - The slave connects to the pipe
 *
 *
 * Compiling
 * ---------
 *
 *	$ gcc -Wall -o worker worker.c
 */

#include <stdio.h>
#include <signal.h>
#include <unistd.h> /* write(), sleep() */
#include <fcntl.h>  /* open() */


int main(void)
{
	int fd_r = -1;
	int rdstat = 0;
	char rdbuf[2] = {'\0', '\0'};

	do {
		fd_r = open("MYFIFO", O_RDONLY);

		if (fd_r == -1) {
			printf("Worker - Pipe MYFIFO not found, please start master program\n");
			sleep(2);
		}
	} while (fd_r == -1);

	do {
		rdstat = read(fd_r, rdbuf, 2);
		if (rdstat != 0) {
			printf("Worker - Received: '%c'\n", rdbuf[0]);
		}
	} while (rdstat > 0);

	unlink("MYFIFO");
	return 0;
}


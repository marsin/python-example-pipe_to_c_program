#! /usr/bin/env python3

# Links
# -----
#
#  - <https://www.tutorialspoint.com/python3/os_write.htm>
#  - <https://bytes.com/topic/python/answers/519603-python-open-named-pipe-hanging>
#  - <https://stackoverflow.com/questions/227459/how-to-get-the-ascii-value-of-a-character>
#  - <https://stackoverflow.com/questions/21017698/converting-int-to-bytes-in-python-3>
#

import os, sys


os.mkfifo('MYFIFO')

fd_w = os.open('MYFIFO', os.O_WRONLY)
#fd_r = os.open('MYFIFO', os.O_RDONLY)

if fd_w != 0:
    print("Commander - Sending: 'A'")
    os.write(fd_w, str.encode('A'))

    print("Commander - Sending: 'A'")
    os.write(fd_w, b'\x41')

    os.close(fd_w)


Python - Example - Pipe to C Program
====================================

 This is a simple example demonstrating a communication
 between a Python script and an C program, using a named pipe.


About
-----

 - The C program is the slave (worker process)
 - The Python program is the master (e.g. a GUI program)
 - The communication between the programs is realized by a named pipe
 - The master initiates a pipe
 - The slave connects to the pipe


References
----------

### Python

 - <https://www.tutorialspoint.com/python3/os_write.htm>
 - <https://bytes.com/topic/python/answers/519603-python-open-named-pipe-hanging>
 - <https://stackoverflow.com/questions/227459/how-to-get-the-ascii-value-of-a-character>
 - <https://stackoverflow.com/questions/21017698/converting-int-to-bytes-in-python-3>


### C

 - <https://www.rheinwerk-verlag.de/linux-unix-programmierung_3854/>
 - <https://www.hanser-fachbuch.de/buch/Linux+Hardware+Hackz/9783446413627>


Compiling
---------

```sh
$ make
$ make clean
```


Usage
-----

 1. Compile the 'worker' program (`make`)
 2. Open two terminals ('Terminal A' and 'Terminal B') and change to the `src` directory
 3. Start the 'commander' process with command `./commander.py` in 'Terminal A'
    - The 'commander' creates a named pipe, called `MYFIFO`
    - By switching to 'Terminal B' and with the `ls -l` command you will see the pipe file
 4. Start the 'worker' process in 'Terminal B' with command `./worker`
    - The 'worker' connects to the FIFO pipe
    - The commander pushes an 'A' into the FIFO and the worker receives this character
    - The 'worker' disconnects and the 'commander' removes the pipe

